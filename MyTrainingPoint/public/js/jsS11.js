var PASS_DEFINE = '07101997';
var inputOldPassword=document.getElementById('matkhau1');
var inputNewPassword=document.getElementById('matkhau2');
var inputNewPassword2=document.getElementById('matkhau3');

var formLogin=document.getElementById('form-login');

if (formLogin.attachEvent)
{
    formLogin.attachEvent('submit', onFormSubmit);
}
else
{
    formLogin.addEventListener('submit', onFormSubmit);
}

function onFormSubmit(e)
{
    if (e.preventDefault())e.preventDefault();
    var oldPassword=inputOldPassword.value;
    var newPassword=inputNewPassword.value;
    var newPassword2=inputNewPassword2.value;

    var n=oldPassword.localeCompare(PASS_DEFINE);
    var m=newPassword.localeCompare(newPassword2);
    if (n!=0)
    {
        var divErrors= document.getElementById('errors0');
        divErrors.innerHTML =
            '<div>\n' +
            '<p style="color: #525252; font-family: SVN-Gilroy; font-weight: bold; font-size: 18px; margin-left: 10px; margin-top: -10px;">Mật khẩu không chính xác</p>\n' +
            '</div>'
    }
    else if (newPassword.length==0 || newPassword2.length==0)
    {
        var divErrors1= document.getElementById('errors1');
        divErrors1.innerHTML =
            '<div>\n' +
            '<p style="color: #525252; font-family: SVN-Gilroy; font-weight: bold; font-size: 18px; margin-left: 10px; margin-top: -10px;">Điền đầy đủ thông tin vào ô trống</p>\n' +
            '</div>'
    }
    else if (m!=0)
    {
        var divErrors2= document.getElementById('errors2');
        divErrors2.innerHTML =
            '<div>\n' +
            '<p style="color: #525252; font-family: SVN-Gilroy; font-weight: bold; font-size: 18px; margin-left: 10px; margin-top: -10px; animation-delay: 2s">Mật khẩu mới không trùng khớp</p>\n' +
            '</div>'
    }
    else
    {
        PASS_DEFINE=newPassword;
        var divSuc= document.getElementById('success');
        divSuc.innerHTML =
            '<div>\n' +
            '<p style="color: #525252; font-family: SVN-Gilroy; font-weight: bold; font-size: 18px; margin-left: 10px; margin-top: -10px">Đổi mật khẩu thành công</p>\n' +
            '</div>'
    }
    return false;
}